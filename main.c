#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h> //
#define MAXLEN 200
#define FIFO "/tmp/fifo" 

//подсчет количества строк в файле
int CalcCountStr(char*fileName)
{
	FILE*file;
	char string[MAXLEN];
	int countStr = 0;
	if((file = fopen(fileName, "r")) == NULL){
		printf("Don't open file for reading");
		exit(-2);
	}
	while(fgets(string,sizeof(string),file)){
		countStr++;
		//printf("%s\n", string);
	}
	fclose(file);
	return(countStr);
}
//сравнение строк
int compareStr(char *str1, char str2[])
{
	if(strcmp(str1, str2) == 0){
    	return 0;
    }
 	return 1;
}
//чтение из потока
void readFIFO(int i, char *findStr)
{
	int readfd, len;
	ssize_t n;
	char buff[MAXLEN];
	len = strlen(FIFO);
	char nameFIFO[len + 1];
	sprintf(nameFIFO,"%s.%d", FIFO, i);
	readfd = open(nameFIFO, O_RDONLY, 0);
	while((n = read(readfd, buff, MAXLEN)) > 0);
	close(readfd);
	exit(compareStr(findStr, buff));
}
//запись в поток
int writeFIFO(char string[], int pos, int i)
{
	int writefd;
	int len;
	len = strlen(FIFO);
	char nameFIFO[len + 1];
	sprintf(nameFIFO,"%s.%d", FIFO, i);
	mkfifo(nameFIFO, 0664);
	writefd = open(nameFIFO, O_WRONLY, 0);
	write(writefd, string, pos);
	close(writefd);
	return(0);
}
int findStr(char *fileName, char *findStr)
{
	FILE*file;
	int countStr = 0, pos, status;
	char string[MAXLEN] = "";
	countStr = CalcCountStr(fileName);
	printf("Count stings in file: %d\n", countStr);
	pid_t pid[countStr];
	if((file = fopen(fileName, "r")) == NULL){
		printf("Don't open file for reading");
		exit(-2);
	}
	//rewind(file);
	for(int i = 0; i < countStr; i++){
		if((fscanf(file, " %[^\n]", string)) != EOF)
			printf("%s\n", string);
        pos = strlen(string);
        string[pos + 1] = '\0';
       	pos++;
   
        pid[i] = fork();
		
        if (-1 == pid[i]) {
            perror("fork"); //произошла ошибка
            exit(-3); //выход из родительского процесса
        } 
        else if (0 == pid[i]) {
        		printf("Child: it's %d process start!\n", i);
        		readFIFO(i, findStr);
        		printf("Error with start process\n");
        		exit(-2);
        	}
        	else{
        		writeFIFO(string, pos, i);
        	}     
	}
	// если выполняется родительский процесс
    printf("It's parent!\n");
    // ожидание окончания выполнения всех запущенных процессов
    for (int i = 0; i < countStr; i++) {
    	if( wait(&status) == -1){
    		perror("wait error");
    	}else if (WIFEXITED(status)){
    		printf("process-child %d done,  result=%d\n", i, WEXITSTATUS(status));
    	} else {
    		perror("PARENT: потомок не завершился успешно");
    	}
    }
    printf("Result:\n0 - process-child found the string\n1 - process-child did not find the string\n");
	fclose(file);
	return(0);
}
int main(int argc, char *argv[])
{
	if(argc < 3){
		printf("Error: need more arguments\n");
		exit(-1);
	}
	else{
		printf("Find string: %s\n", argv[2]);
		findStr(argv[1], argv[2]);
	}

	return(0);
}